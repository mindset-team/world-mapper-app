#! /usr/bin/env python3

from pathlib import Path
from pprint import pprint
import typing as tp

import geopandas as gpd
import numpy as np
from shapely.geometry import Point
import shapely

LONGITUDE_DEF_RES = 121
LATITUDE_DEF_RES = 61


def shape_fn(land_res: int = 110) -> Path:
    """
        returns the shape file name by resolution from shape_files folder
        shapefiles are found
            here: naturalearthdata.com/downloads/[110|50|10]m-physical-vectors/

        land or water condition found using 'land' shape files
    """
    fn = f"ne_{land_res}m_land.shp"
    cwd = Path(__file__).parents[0]
    return cwd.joinpath("shape_files", fn)


def calc_inland_df(
    land_res: int = 110,
    lng_range: tp.Tuple = (-180, 180, LONGITUDE_DEF_RES),
    lat_range: tp.Tuple = (-55, 90, LATITUDE_DEF_RES),
) -> gpd.geodataframe.GeoDataFrame:
    world_df = gpd.read_file(shape_fn(land_res=land_res))
    ll_rng = np.array([lng_range[:2], lat_range[:2]])
    ll_res = [lng_range[2], lat_range[2]]
    lng, lat = [np.linspace(*ll_rng[_], ll_res[_]) for _ in range(2)]
    in_land_lst = [np.nan] * world_df.shape[0]
    for en, geo in enumerate(world_df.geometry):
        blng = lng[(lng >= geo.bounds[0]) & (lng <= geo.bounds[2])]
        if blng.size == 0:
            continue
        blat = lat[(lat >= geo.bounds[1]) & (lat <= geo.bounds[3])]
        if blat.size == 0:
            continue
        mgrid = np.array(np.meshgrid(blng, blat)).T.reshape(-1, 2)
        in_land = np.array([c for c in mgrid if geo.contains(Point(c))])
        if in_land.size:
            in_land_lst[en] = in_land
    world_df["in_land"] = in_land_lst
    return world_df


def unique_inland_coords(
    world_df: gpd.geodataframe.GeoDataFrame,
    precision: tp.Union[None, int] = None,
) -> np.ndarray:
    coords = np.vstack(world_df.in_land.dropna())
    if precision:
        coords = np.round(coords, precision)
    coords = np.unique(coords, axis=0)
    return coords


def main(
    land_res: int = 110,
    lng_range: tp.Union[tp.Tuple, None] = None,
    lat_range: tp.Union[tp.Tuple, None] = None,
    include_antartica: bool = False,
    coord_precision: int = None,
) -> tp.Dict:
    lng_range = (
        [int(w) for w in lng_range.split(",")]
        if lng_range and isinstance(lng_range, str)
        else (-180, 180, LONGITUDE_DEF_RES)
    )
    lat_range = (
        [int(w) for w in lat_range.split(",")]
        if lat_range and isinstance(lat_range, str)
        else (-90 if include_antartica else -55, 90, LATITUDE_DEF_RES)
    )
    land_res = int(land_res) if land_res else 110
    world_df = calc_inland_df(
        land_res=land_res, lng_range=lng_range, lat_range=lat_range,
    )
    coord_precision = int(coord_precision) if coord_precision else None
    coords = unique_inland_coords(world_df, precision=coord_precision)
    params = {
        "lng_range": lng_range,
        "lat_range": lat_range,
        "include_antartica": include_antartica,
        "coord_precision": coord_precision,
    }
    resp = {
        "inland_coords": coords.tolist(),
        "inland_count": coords.shape[0],
        "inland_ratio": coords.shape[0] / (lng_range[2] * lat_range[2]),
        "params": params,
    }
    return resp


if __name__ == "__main__":  # Local testing
    coord_dct = main(
        lat_range="-40,40,11", 
        coord_precision=5, 
        include_antartica="False"
    )
    pprint(coord_dct)
